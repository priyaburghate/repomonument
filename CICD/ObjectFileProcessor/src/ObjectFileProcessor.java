import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

public class ObjectFileProcessor {
	//Read Ids on Source Org
	public   Map<String,String> readSourceOrgIds(String filename , String uniqueID){
		Map<String,String> idToNameMap = new HashMap<String,String>();
		//System.out.println(filename);
		try(CSVReader reader = new CSVReader(new FileReader(filename))){
			
			String values[] = reader.readNext();
			int uniqueIdIndex = getIndex(uniqueID, values);
			//System.out.println("Index of "+uniqueID+" : "+uniqueIdIndex);
			  while ((values = reader.readNext()) != null) {			       
				  idToNameMap.put(values[0],values[uniqueIdIndex]);				   
			   }
			   
			   System.out.println(idToNameMap);
			   reader.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		return idToNameMap;
	}
	
	//Read Ids on target Org
	public  Map<String,String> readTargetOrgIds(String filename , String uniqueID){
		Map<String,String> nameToIdMap = new HashMap<String,String>();
	 	try (CSVReader reader = new CSVReader(new FileReader(filename))) {
	 		String values[] = reader.readNext();
			int uniqueIdIndex = getIndex(uniqueID, values);
			//System.out.println("Index of "+uniqueID+" : "+uniqueIdIndex);		
			   while ((values = reader.readNext()) != null) {			       
				   nameToIdMap.put(values[uniqueIdIndex],values[0]);				   
			   }
			  System.out.println(nameToIdMap);
			  reader.close();
			}
	 	catch (IOException e) {
				e.printStackTrace();
			}
		return nameToIdMap;
	}
	
	//Maps old ID to new ID
	public  Map<String , String> mapOldIDtoNew( Map<String , String> idToNameMap , Map<String , String> nameToIdMap){
		Map<String,String> oldToNewIdMap = new HashMap<String,String>();
		for(String key:idToNameMap.keySet()){
			String val = null;
			val = nameToIdMap.get(idToNameMap.get(key));
			oldToNewIdMap.put(key ,val);
		}
		System.out.println(oldToNewIdMap);
		return oldToNewIdMap;
	}
	
	// Returns the index of the given column name
	private  int getIndex(String columnName, String[] columns) {
		int index = -1;
			for(int j = 0 ; j < columns.length;j++){
				if(columns[j].equals(columnName)){
					index = j;
					break;				
				}
			}
		
		return index;	
	}

	
	//Replaces old ids with new Ids
	private void replaceIds(Map<String, String> oldToNewIdMap, String fieldName,String fileName) {
	
		List<String[]> outputLines = readSourceFile(fileName,oldToNewIdMap,fieldName);
		writeSourceFile(fileName,outputLines);
	}

	private List<String[]> readSourceFile(String fileName,Map<String, String> oldToNewIdMap, String fieldName) {
		List<String[]> outputLines = new ArrayList<String[]>();
		

		try (CSVReader reader = new CSVReader(new FileReader(fileName))) {
			String values[] = reader.readNext();
			int uniqueIdIndex = getIndex(fieldName, values);
			//System.out.println("Index of "+fieldName+" : "+uniqueIdIndex);
			outputLines.add(values);  
			while ((values = reader.readNext()) != null) {		
				  String temp = oldToNewIdMap.get(values[uniqueIdIndex]);
				   if(temp == null){
					   temp = "";
				   }
				   values[uniqueIdIndex] = temp;
				   outputLines.add(values);
			 }
			reader.close();
		}
		catch (IOException e) {
			e.printStackTrace();				
		}
		return outputLines;
	}

	private void writeSourceFile(String fileName, List<String[]> outputLines) {
		//System.out.println("Inside writer");
		CSVWriter writer =null;
		try{			
		 writer = new CSVWriter(new FileWriter(fileName));
	       	  for(String str[]: outputLines){
	       		  writer.writeNext(str);
			}
	     
		  } catch (IOException e) {
			e.printStackTrace();			
		}
		finally {
			  try {
				writer.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	
	}

	public static void main(String[] args) {
		ObjectFileProcessor e1 = new ObjectFileProcessor();
		String sourceIdFile = args[0];
		String targetIdFile = args[1];
		String sourceDataFile = args[2];
		String fieldName = args[3];
		String uniqueID = args[4];
		Map<String,String> idToNameMap = e1.readSourceOrgIds(sourceIdFile,uniqueID);
		Map<String,String> nameToIdMap = e1.readTargetOrgIds(targetIdFile,uniqueID);
		Map<String,String> oldToNewIdMap=e1.mapOldIDtoNew(idToNameMap,nameToIdMap);
		e1.replaceIds(oldToNewIdMap,fieldName,sourceDataFile);
	}


}
