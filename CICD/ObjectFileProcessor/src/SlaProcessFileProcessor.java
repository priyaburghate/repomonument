import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

public class SlaProcessFileProcessor {
	
	public String processSlaProcessName(String slaProcessName){
		String name = null;
		
		if(slaProcessName.contains("_v")){
			String[] temp = slaProcessName.split("_v");
			if(temp.length == 2 && temp[1].matches("[0-9]+")){
				name = temp[0];
			}
			else{
				if(temp[temp.length-1].matches("[0-9]+") && temp[temp.length-2].matches("[0-9]+") ){
					String tempStr ="";
					for(int i = 0; i < temp.length - 3 ; i++){
						tempStr+=temp[i]+"_v"; 
					}
					tempStr+= temp[temp.length-3];
					name = tempStr;
				}
				else if(temp[temp.length-1].matches("[0-9]+")){
					String tempStr ="";
					for(int i = 0; i < temp.length - 2 ; i++){
						tempStr+=temp[i]+"_v"; 
					}
					tempStr+= temp[temp.length-2];
					name = tempStr;
				}
				else
				{
					
					name = slaProcessName;
				}
			}
		}
		else{
			name =slaProcessName;
		}
		System.out.println("SLA Process name before cleanup: "+slaProcessName);
		System.out.println("SLA Process name after cleanup: "+name);
		return name;
	}
	//Read Ids on Source Org
	public   Map<String,String> readSourceOrgIds(String filename , String uniqueID){
		Map<String,String> idToNameMap = new HashMap<String,String>();
		//System.out.println(filename);
		try(CSVReader reader = new CSVReader(new FileReader(filename))){
			
			String values[] = reader.readNext();
			int uniqueIdIndex = getIndex(uniqueID, values);
			//System.out.println("Index of "+uniqueID+" : "+uniqueIdIndex);
			String tempUniqueID;
			  while ((values = reader.readNext()) != null) {	
				  tempUniqueID = processSlaProcessName(values[uniqueIdIndex]);
				  idToNameMap.put(values[0],tempUniqueID);				   
			   }
			   
			  // System.out.println(idToNameMap);
			   reader.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		return idToNameMap;
	}
	
	//Read Ids on target Org
	public  Map<String,String> readTargetOrgIds(String filename , String uniqueID){
		Map<String,String> nameToIdMap = new HashMap<String,String>();
	 	try (CSVReader reader = new CSVReader(new FileReader(filename))) {
	 		String values[] = reader.readNext();
			int uniqueIdIndex = getIndex(uniqueID, values);
			//System.out.println("Index of "+uniqueID+" : "+uniqueIdIndex);		
			String tempUniqueID;
			   while ((values = reader.readNext()) != null) {			       
				   tempUniqueID = processSlaProcessName(values[uniqueIdIndex]);
				   nameToIdMap.put(tempUniqueID,values[0]);				   
			   }
			  //System.out.println(nameToIdMap);
			  reader.close();
			}
	 	catch (IOException e) {
				e.printStackTrace();
			}
		return nameToIdMap;
	}
	
	//Maps old ID to new ID
	public  Map<String , String> mapOldIDtoNew( Map<String , String> idToNameMap , Map<String , String> nameToIdMap){
		Map<String,String> oldToNewIdMap = new HashMap<String,String>();
		for(String key:idToNameMap.keySet()){
			String val = null;
			val = nameToIdMap.get(idToNameMap.get(key));
			oldToNewIdMap.put(key ,val);		
		}
		//System.out.println(oldToNewIdMap);
		return oldToNewIdMap;
	}
	
	// Returns the index of the given column name
	private  int getIndex(String columnName, String[] columns) {
		int index = -1;
			for(int j = 0 ; j < columns.length;j++){
				if(columns[j].equalsIgnoreCase(columnName)){
					index = j;
					break;				
				}
			}
		
		return index;	
	}

	
	//Replaces old ids with new Ids
	private void replaceIds(Map<String, String> oldToNewIdMap, String fieldName,String fileName) {
	
		List<String[]> outputLines = readSourceFile(fileName,oldToNewIdMap,fieldName);
		writeSourceFile(fileName,outputLines);
	}

	private List<String[]> readSourceFile(String fileName,Map<String, String> oldToNewIdMap, String fieldName) {
		List<String[]> outputLines = new ArrayList<String[]>();
		

		try (CSVReader reader = new CSVReader(new FileReader(fileName))) {
			String values[] = reader.readNext();
			int uniqueIdIndex = getIndex(fieldName, values);
			System.out.println("Index of "+fieldName+" : "+uniqueIdIndex);
			outputLines.add(values);  
			while ((values = reader.readNext()) != null) {		
				  String temp = oldToNewIdMap.get(values[uniqueIdIndex]);
				   if(temp == null){
					   temp = "";
				   }
				
				   System.out.println(values[uniqueIdIndex]+" replaced with "+temp);
				   values[uniqueIdIndex] = temp;
				   outputLines.add(values);
				   
			 }
			reader.close();
		}
		catch (IOException e) {
			e.printStackTrace();				
		}
		return outputLines;
	}

	private void writeSourceFile(String fileName, List<String[]> outputLines) {
		//System.out.println("Inside writer");
		CSVWriter writer =null;
		try{			
		 writer = new CSVWriter(new FileWriter(fileName));
	       	  for(String str[]: outputLines){
	       		  writer.writeNext(str);
			}
	     
		  } catch (IOException e) {
			e.printStackTrace();			
		}
		finally {
			  try {
				writer.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	
	}

	public static void main(String[] args) throws Exception {
		SlaProcessFileProcessor e1 = new SlaProcessFileProcessor();
	
		if(args[0] == "" || args[0].isEmpty()){
			throw new Exception("Parameter SourceIDFile Invalid or Missing");
		}
		if(args[1] == "" || args[1].isEmpty()){
			throw new Exception("Parameter targetIdFile Invalid or Missing");
		}
		if(args[2] == "" || args[2].isEmpty()){
			throw new Exception("Parameter sourceDataFile Invalid or Missing");
		}
		if(args[3] == "" || args[3].isEmpty()){
			throw new Exception("Parameter accountFile Invalid or Missing");
		}

		if(args[4] == "" || args[4].isEmpty()){
			throw new Exception("Parameter accountName Invalid or Missing");
		}


		String sourceIdFile = args[0];
		String targetIdFile = args[1];
		String sourceDataFile = args[2];
		String accountFile = args[3];
		String accountName = args[4];
		String fieldName = "SLAPROCESSID";
		String uniqueID = "NAMENORM";
		String accountID = e1.getAccountID(accountFile,accountName);
		Map<String,String> idToNameMap = e1.readSourceOrgIds(sourceIdFile,uniqueID);
		System.out.println("idToNameMap:");
		System.out.println(idToNameMap);
		Map<String,String> nameToIdMap = e1.readTargetOrgIds(targetIdFile,uniqueID);
		System.out.println("nameToIdMap");
		System.out.println(nameToIdMap);
		Map<String,String> oldToNewIdMap=e1.mapOldIDtoNew(idToNameMap,nameToIdMap);
		System.out.println(oldToNewIdMap);
		e1.replaceIds(oldToNewIdMap,fieldName,sourceDataFile);		 		
		e1.populateAccountID(sourceDataFile,accountID);
	}
	private void populateAccountID(String fileName, String accountID) {
		// TODO Auto-generated method stub
		List<String[]> outputLines = new ArrayList<String[]>();
		try (CSVReader reader = new CSVReader(new FileReader(fileName))) {
			String values[] = reader.readNext();
			int accountIdIndex = getIndex("ACCOUNTID", values);
			outputLines.add(values);  
			while ((values = reader.readNext()) != null) {		
					values[accountIdIndex] = accountID;
				   outputLines.add(values);
				   
			 }
			reader.close();
		}
		catch (IOException e) {
			e.printStackTrace();				
		}
		writeSourceFile(fileName, outputLines);
	}
	private String getAccountID(String accountFile,String accountName) {
		String accountID = "";
	try(CSVReader reader = new CSVReader(new FileReader(accountFile))){
			
			String values[] = reader.readNext();
			int nameIndex = getIndex("NAME", values);	
			  while ((values = reader.readNext()) != null) {	
					if(values[nameIndex].equalsIgnoreCase(accountName)){
						accountID = values[0];
						break;
					}
			   }
	
			   reader.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		return accountID;
	}


}
