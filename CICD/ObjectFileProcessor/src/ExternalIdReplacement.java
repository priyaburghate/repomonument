import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

public class ExternalIdReplacement {
	//Read Ids on Source Org
	public   Map<String,String> readSourceOrgIds(String filename , String uniqueID){
		Map<String,String> idToNameMap = new HashMap<String,String>();
		//System.out.println(filename);
		try(CSVReader reader = new CSVReader(new FileReader(filename))){
			
			String values[] = reader.readNext();
			int uniqueIdIndex = getIndex(uniqueID, values);
			//System.out.println("Index of "+uniqueID+" : "+uniqueIdIndex);
			  while ((values = reader.readNext()) != null) {			       
				  idToNameMap.put(values[0],values[uniqueIdIndex]);				   
			   }
			   
			  // System.out.println(idToNameMap);
			   reader.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		return idToNameMap;
	}
	
	
	
	// Returns the index of the given column name
	private  int getIndex(String columnName, String[] columns) {
		int index = -1;
			for(int j = 0 ; j < columns.length;j++){
				if(columns[j].equals(columnName)){
					index = j;
					break;				
				}
			}
		
		return index;	
	}

	
	//Replaces old ids with new Ids
	private void replaceIds(Map<String, String> oldToNewIdMap, String fieldName,String fileName) {
	
		List<String[]> outputLines = readSourceFile(fileName,oldToNewIdMap,fieldName);
		writeSourceFile(fileName,outputLines);
	}

	private List<String[]> readSourceFile(String fileName,Map<String, String> oldToNewIdMap, String fieldName) {
		List<String[]> outputLines = new ArrayList<String[]>();
		

		try (CSVReader reader = new CSVReader(new FileReader(fileName))) {
			String values[] = reader.readNext();
			int uniqueIdIndex = getIndex(fieldName, values);
			//System.out.println("Index of "+fieldName+" : "+uniqueIdIndex);
			outputLines.add(values);  
			while ((values = reader.readNext()) != null) {		
				  String temp = oldToNewIdMap.get(values[uniqueIdIndex]);
				   if(temp == null){
					   temp = "";
				   }
				
				   System.out.println(values[uniqueIdIndex]+" replaced with "+temp);
				   values[uniqueIdIndex] = temp;
				   outputLines.add(values);
				   
			 }
			reader.close();
		}
		catch (IOException e) {
			e.printStackTrace();				
		}
		return outputLines;
	}

	private void writeSourceFile(String fileName, List<String[]> outputLines) {
		//System.out.println("Inside writer");
		CSVWriter writer =null;
		try{			
		 writer = new CSVWriter(new FileWriter(fileName));
	       	  for(String str[]: outputLines){
	       		  writer.writeNext(str);
			}
	     
		  } catch (IOException e) {
			e.printStackTrace();			
		}
		finally {
			  try {
				writer.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	
	}

	public static void main(String[] args) throws Exception {
		ExternalIdReplacement e1 = new ExternalIdReplacement();
	
		if(args[0] == "" || args[0].isEmpty()){
			throw new Exception("Parameter SourceIDFile Invalid or Missing");
		}
		if(args[1] == "" || args[1].isEmpty()){
			throw new Exception("Parameter sourceDataFile Invalid or Missing");
		}
		if(args[2] == "" || args[2].isEmpty()){
			throw new Exception("Parameter fieldName Invalid or Missing");
		}
		if(args[3] == "" || args[3].isEmpty()){
			throw new Exception("Parameter uniqueID Invalid or Missing");
		}
		
		String sourceIdFile = args[0];
		String sourceDataFile = args[1];
		String fieldName = args[2];
		String uniqueID = args[3];
		Map<String,String> idToNameMap = e1.readSourceOrgIds(sourceIdFile,uniqueID);
		e1.replaceIds(idToNameMap,fieldName,sourceDataFile);
	}


}
